# OpenShift commands

## Node

### Unschedule Nodes

```bash
oc adm  manage-node nodes1 .... --schedulable=false
```

## Pod

### Delete Pods by state

```bash
oc get po --all-namespaces | grep OutOfcpu | awk '{ print "oc delete po "$2" -n "$1 }' | bash
```
## Builds

### Cleanup

[cleanup-builds.sh](bin/openshift/cleanup-builds.sh)

## ImageStreams

### Cleanup istags
[cleanup-istags.sh](bin/openshift/cleanup-istags.sh)