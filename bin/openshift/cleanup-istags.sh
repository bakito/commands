#!/bin/bash

for istag in $( oc get istag -o name ); do

#   echo "istag: ${istag}"

   creationTime="$(oc get ${istag} -o jsonpath='{.metadata.creationTimestamp}' )"

 #  echo "creationTime: ${creationTime}"

   if [[ "$( date -d ${creationTime} +%s)" -lt  "$( date -d 7 +%s )" ]]; then
      oc delete ${istag}
   fi
done
