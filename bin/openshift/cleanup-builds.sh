#!/bin/bash

for build in $( oc get builds -o name ); do

   STATE=$(oc get "${build}" -o jsonpath="{.status.phase}")

   if [[ "${STATE}" != "Complete" ]] && [[ "${STATE}" != "Failed" ]]; then
       continue
   fi

   #echo "build: ${build}"

   completionTime="$(oc get ${build} -o jsonpath='{.status.completionTimestamp}' )"

   #echo "completionTime: ${completionTime}"

   if [[ "$( date -d ${completionTime} +%s)" -lt  "$( date -d 1 +%s )" ]]; then
      oc delete ${build}
   fi
done
