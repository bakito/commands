#!/bin/bash

IGNORED_CONTAINER_PATTERN=svn2git

sudo /usr/bin/docker ps -a --filter "status=created" | grep -v "CONTAINER ID"
if [[ "$?" == "0"  ]]; then
    read -p "Delete created? [y/n] " yn
    case $yn in
        [Yy]* ) sudo /usr/bin/docker rm $(sudo /usr/bin/docker ps -a --filter "status=created" -q) ;;
        [Nn]* ) ;;
        * ) echo "Please answer yes or no.";;
    esac
fi

sudo /usr/bin/docker ps -a --filter "status=dead" | grep -v "CONTAINER ID"
if [[ "$?" == "0"  ]]; then
    read -p "Delete dead? [y/n] " yn
    case $yn in
        [Yy]* ) sudo /usr/bin/docker rm $(sudo /usr/bin/docker ps -a --filter "status=dead" -q) ;;
        [Nn]* ) ;;
        * ) echo "Please answer yes or no.";;
    esac
fi

sudo /usr/bin/docker ps -a --filter "status=exited" | grep -v "CONTAINER ID" | grep -v "${IGNORED_CONTAINER_PATTERN}"
if [[ "$?" == "0"  ]]; then
     read -p "Delete exited? [y/n] " yn
     case $yn in
            [Yy]* ) sudo /usr/bin/docker rm $(sudo /usr/bin/docker ps -a --filter "status=exited" | grep -v "CONTAINER ID" | grep -v "${IGNORED_CONTAINER_PATTERN}" | awk '{print $1}') ;;
            [Nn]* ) ;;
            * ) echo "Please answer yes or no.";;
    esac
fi

