# Docker

## Images

### Delete untagged images
```bash
docker rmi $(docker images | grep "<none>"  | awk '{print $3}')

docker rmi -f $(docker images -f "dangling=true" -q)
```

## remove stopped containers

### Container
[removeStoppedContainers.sh](bin/docker/removeStoppedContainers.sh)